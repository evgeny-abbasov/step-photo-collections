package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class CommentRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;

    @BeforeEach
    private void setUp() {
        dbCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        //given
        UserEntity user = EntityUtils.prepareUser();
        userRepository.create(user);

        PhotoEntity photo = EntityUtils.preparePhoto();
        photoRepository.create(photo);

        CommentEntity commentToSave = EntityUtils.prepareComment(user,photo);

        //when
        CommentEntity savedComment = commentRepository.create(commentToSave);

        //then
        Assertions.assertNotNull(savedComment.getId());
        CommentEntity foundComment = commentRepository.findById(savedComment.getId());

       Assertions.assertNotNull(foundComment.getMessage());
       Assertions.assertEquals(user.getId(), foundComment.getUser().getId());
       Assertions.assertEquals(photo.getId(), foundComment.getPhoto().getId());
    }

    @Test
    public void findAll_happyPath() {
        //given
        UserEntity user = EntityUtils.prepareUser();
        PhotoEntity photo = EntityUtils.preparePhoto();

        userRepository.create(user);
        photoRepository.create(photo);

        CommentEntity comment1 = EntityUtils.prepareComment(user,photo);
        CommentEntity comment2 = EntityUtils.prepareComment(user,photo);
        CommentEntity comment3 = EntityUtils.prepareComment(user,photo);

        commentRepository.create(comment1);
        commentRepository.create(comment2);
        commentRepository.create(comment3);

        //when
        List<CommentEntity> foundComments = commentRepository.findAll();

        //then
        Assertions.assertEquals(3, foundComments.size());
    }

}
