package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.mapper.PhotoMapper;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionRepository collectionRepository;

    @Autowired
    private PhotoMapper mapper;

    public PhotoFullDto findById(int id) {
        PhotoEntity photo = photoRepository.findById(id);

        if (photo == null) {
            throw new RuntimeException("Photo not found by id: " + id);
        }
        System.out.println("PhotoService -> Found photo: " + photo);
        PhotoFullDto dto = mapper.map(photo);
        return dto;
    }

    public List<PhotoShortDto> findAll() {
        List<PhotoEntity> allPhotos = photoRepository.findAll();
        System.out.println("PhotoService -> Found photos: " + allPhotos);
        List<PhotoShortDto> dtos = mapper.map(allPhotos);
        return dtos;
    }

    public PhotoFullDto update(PhotoUpdateDto updateRequest) {
        PhotoEntity photo = photoRepository.findById(updateRequest.getId());
        if (photo == null) {
            throw new RuntimeException("PhotoService -> Photo not found by id: " + photo.getId());
        }

        photo.setName(updateRequest.getName());

        PhotoEntity updatedPhoto = photoRepository.update(photo);
        System.out.println("PhotoService -> Updated photo: " + photo);
        PhotoFullDto dto = mapper.map(photo);
        return dto;
    }

    public PhotoFullDto create(PhotoCreateDto createRequest) {
        PhotoEntity photo = mapper.map(createRequest);
        if (photo.getId() != null) {
            throw new RuntimeException("PhotoService -> " + "Can't create entity which already has id");
        }
        PhotoEntity createdPhoto = photoRepository.create(photo);
        System.out.println("PhotoService -> photo was created: " + createdPhoto);
        PhotoFullDto createdDto = mapper.map(createdPhoto);
        return createdDto;
    }

    public void deleteById(int id) {
        PhotoEntity photo = photoRepository.findById(id);
        List<CollectionEntity> photoCollections = photo.getCollections();

        for (CollectionEntity collection : photoCollections) {
            collection.getPhotos().remove(photo);
            collectionRepository.update(collection);
        }
        photoRepository.deleteById(id);
    }
}
