package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.CommentCreateDto;
import by.itstep.collectionsphotos.dto.CommentFullDto;
import by.itstep.collectionsphotos.dto.CommentUpdateDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.repository.CommentRepository;
import by.itstep.collectionsphotos.repository.PhotoRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CommentMapper mapper;

    public CommentFullDto findById(int id) {
        CommentEntity comment = commentRepository.findById(id);

        if (comment == null) {
            throw new RuntimeException("comment not found by id: " + id);
        }
        System.out.println("CommentService -> Found comment: " + comment);
        CommentFullDto dto = mapper.map(comment);
        return dto;
    }

    public List<CommentFullDto> findAll() {
        List<CommentEntity> allComments = commentRepository.findAll();
        System.out.println("CommentService -> Found comments: " + allComments);
        List<CommentFullDto> dtos = mapper.map(allComments);
        return dtos;
    }

    public CommentFullDto update(CommentUpdateDto updateRequest) {
      CommentEntity entityToUpdate = commentRepository.findById(updateRequest.getId());

      entityToUpdate.setMessage(updateRequest.getMessage());

      CommentEntity comment = commentRepository.update(entityToUpdate);

      CommentFullDto dto = mapper.map(comment);
      return dto;
    }

    public CommentFullDto create(CommentCreateDto createRequest) {
        CommentEntity entity = mapper.map(createRequest);

        PhotoEntity photo = photoRepository.findById(createRequest.getPhotoId());
        UserEntity user = userRepository.findById(createRequest.getUserId());

        entity.setPhoto(photo);
        entity.setUser(user);

        CommentEntity comment = commentRepository.create(entity);

        CommentFullDto createdDto = mapper.map(comment);
        return createdDto;
    }

    public void deleteById(int id) {
        CommentEntity commentToDelete = commentRepository.findById(id);
        if (commentToDelete == null) {
            throw new RuntimeException("Comment was not found by id: " + id);
        }
        commentRepository.deleteById(id);
        System.out.println("CommentService -> Deleted comment: " + commentToDelete);
    }

}
