package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.*;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.mapper.CollectionMapper;
import by.itstep.collectionsphotos.mapper.CommentMapper;
import by.itstep.collectionsphotos.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionService {

    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CollectionMapper mapper;

    public CollectionFullDto findById(int id) {
        CollectionEntity collection = collectionRepository.findById(id);
        if (collection == null) {
            throw new RuntimeException("Collection not found by id: " + id);
        }
        System.out.println("CollectionService -> Found collection: " + collection);
        CollectionFullDto dto = mapper.map(collection);
        return dto;

    }

    public List<CollectionShortDto> findAll() {
        List<CollectionEntity> allCollections = collectionRepository.findAll();
        System.out.println("CollectionService -> Found collections: " + allCollections);
        List<CollectionShortDto> dtos = mapper.map(allCollections);
        return dtos;
    }

    public CollectionFullDto update(CollectionUpdateDto updateRequest) {
        CollectionEntity collection = collectionRepository.findById(updateRequest.getId());
        if (collection.getId() == null) {
            throw new RuntimeException("CollectionService -> Can't update entity without id");
        }
        if (collectionRepository.findById(collection.getId()) == null) {
            throw new RuntimeException("CollectionService -> Collection not found by id: " + collection.getId());
        }
        if (userRepository.findById(collection.getUser().getId()) == null) {
            throw new RuntimeException("CollectionService -> User not found by id: " + collection.getUser().getId());
        }
        collection.setName(updateRequest.getName());
        collection.setDescription(updateRequest.getDescription());

        CollectionEntity updatedCollection = collectionRepository.update(collection);
        System.out.println("CollectionService -> Updated collection: " + collection);
        CollectionFullDto dto = mapper.map(collection);
        return dto;
    }

    public CollectionFullDto create(CollectionCreateDto createRequest) {
        CollectionEntity collection = mapper.map(createRequest);

        UserEntity user = userRepository.findById(createRequest.getUserId());
        collection.setUser(user);

        if (collection.getId() != null) {
            throw new RuntimeException("CollectionService -> " + "Can't create entity which already has id");
        }
        if (userRepository.findById(collection.getUser().getId()) == null) {
            throw new RuntimeException("CollectionService -> User not found by id: " + collection.getUser().getId());
        }
        CollectionEntity createdCollection = collectionRepository.create(collection);
        System.out.println("CollectionService -> collection was created: " + createdCollection);
        CollectionFullDto createdDto = mapper.map(createdCollection);
        return createdDto;
    }

    public void deleteById(int id) {
        CollectionEntity collectionToDelete = collectionRepository.findById(id);
        if (collectionToDelete == null) {
            throw new RuntimeException("Collection was not found by id: " + id);
        }
        collectionRepository.deleteById(id);
        System.out.println("CollectionService -> Deleted collection: " +  collectionToDelete);

    }

}
