package by.itstep.collectionsphotos.mapper;

import by.itstep.collectionsphotos.dto.*;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CollectionMapper {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PhotoMapper photoMapper;

    public CollectionFullDto map(CollectionEntity entity) {
        CollectionFullDto dto  = new CollectionFullDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setUserId(entity.getUser().getId());

        List<PhotoShortDto> photoDtos = photoMapper.map(entity.getPhotos());
        dto.setPhotos(photoDtos);
        return dto;
    }

    public List<CollectionShortDto> map(List<CollectionEntity> entities) {
        List<CollectionShortDto> dtos = new ArrayList<>();
        for (CollectionEntity entity : entities) {
            CollectionShortDto dto = new CollectionShortDto();
            dto.setId(entity.getId());
            dto.setName(entity.getName());
            dto.setDescription(entity.getDescription());

            dtos.add(dto);
        }
        return dtos;
    }

    public CollectionEntity map(CollectionCreateDto dto) {
        CollectionEntity entity = new CollectionEntity();
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());

        return entity;
    }
}
