package by.itstep.collectionsphotos.dto;

import lombok.Data;

import java.util.List;

@Data
public class CollectionFullDto {

    private Integer id;
    private String name;
    private String description;
    private Integer userId;
    private List<PhotoShortDto> photos;

}
