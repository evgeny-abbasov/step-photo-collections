package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import java.util.List;

public interface CollectionRepository {

    public CollectionEntity findById(int id);

    public List<CollectionEntity> findAll();

    public CollectionEntity create(CollectionEntity entity);

    public CollectionEntity update(CollectionEntity entity);

    public void deleteById(int id);

    public void deleteAll();
}
