package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;

import java.util.List;

public interface UserRepository {

    public UserEntity findById(int id);

    public List<UserEntity> findAll();

    public UserEntity create(UserEntity entity);

    public UserEntity update(UserEntity entity);

    public void deleteById(int id);

    public void deleteAll();

    UserEntity findByUserLogin(String login);
}
