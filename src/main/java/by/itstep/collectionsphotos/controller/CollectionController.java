package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.*;
import by.itstep.collectionsphotos.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CollectionController {

    @Autowired
    private CollectionService collectionService;

    @ResponseBody
    @RequestMapping(value = "/collections", method = RequestMethod.GET)
    public List<CollectionShortDto> findAllCollections() {
        List<CollectionShortDto> allCollections = collectionService.findAll();

        return allCollections;
    }

    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.GET)
    public CollectionFullDto findById(@PathVariable int id) {
        CollectionFullDto collection = collectionService.findById(id);

        return collection;
    }
    @ResponseBody
    @RequestMapping(value = "/collections", method = RequestMethod.POST)
    public CollectionFullDto create(@RequestBody CollectionCreateDto createRequest) {
        CollectionFullDto createdCollection = collectionService.create(createRequest);
        return createdCollection;
    }
    @ResponseBody
    @RequestMapping(value = "/collections", method = RequestMethod.PUT)
    public CollectionFullDto update(@RequestBody CollectionUpdateDto updateRequest) {
        CollectionFullDto updatedCollection = collectionService.update(updateRequest);

        return updatedCollection;
    }
    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {

        collectionService.deleteById(id);
    }



}
